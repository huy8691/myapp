import React, {Component} from 'react';
import './App.css';
import {Segment, Menu} from 'semantic-ui-react';
import {ModuleRegistry, AllCommunityModules} from '@ag-grid-community/all-modules';
import "@ag-grid-community/all-modules/dist/styles/ag-grid.css";
import "@ag-grid-community/all-modules/dist/styles/ag-theme-balham.css";
import {AgGridReact} from "@ag-grid-community/react";

class App extends Component {
    constructor(props) {
        super(props);
        ModuleRegistry.registerModules(AllCommunityModules);
        const columns = [
            {
                headerName: "ID",
                valueGetter: "node.id",
                cellRenderer: "loadingRenderer"
            },
            {field: "name", headerName: "name"},
            {field: "count", headerName: "Count"},
            {field: "required", headerName: "Required"},
            {field: "first", headerName: "First Name"},
            {field: "middle", headerName: "Middle Name"},
            {field: "last", headerName: "Last Name"}
        ]
        this.state = {
            number: 0,
            lastRow: 0,
            rows: [],
            components: {
                loadingRenderer: function (params) {
                    if (params.value !== undefined) {
                        return params.value;
                    } else {
                        return '<img src="https://raw.githubusercontent.com/ag-grid/ag-grid/master/grid-packages/ag-grid-docs/src/images/loading.gif">';
                    }
                }
            },
            defaultColDef: {resizable: true},
            columnDefs: columns,
            rowBuffer: 0,
            rowSelection: "single",
            rowModelType: "infinite",
            cacheBlockSize: 9,
            cacheOverflowSize: 1,
            maxConcurrentDatasourceRequests: 1,
            infiniteInitialRowCount: 1,
            maxBlocksInCache: 1000,
            products: null,
        };
    }

    dataSource = () =>
        ({
            getRows: params => {
                this.getDeals()
                    .then(res => {
                        // this is the array of json objects returned from the getItems call
                        var result = res.data;
                        // i'm keeping an identifier of the last record so on my subsequent call it knows where to start from.
                        var lastKnownKey = result.length - 1;
                        if (result.length > 0) {
                            // i'm also keeping track of the last row so I know where in the grid I'm at
                            var lastRow = this.state.lastRow + result.length;
                            this.setState(state => ({lastRow: lastRow}));

                            // return the results back to the grid where it will append this
                            // new set of data to the end of the records in the grid
                            params.successCallback(result, lastRow);
                            console.log('result1', result)
                            console.log('lastRow', lastRow)
                            this.setState(state => ({number: this.state.number + 1}));
                        } else {
                            params.successCallback([{columnNameField: "No results found."}], 1);
                        }
                        return result;
                    })
                    .catch(error => console.error("Error: ", error));
            }
        });


    getDeals = () => {
        console.log('number', this.state.number)
        // const url = new URL("https://5f999f3e50d84900163b8e96.mockapi.io/api/product");
        let url = new URL(`https://api.instantwebtools.net/v1/passenger?page=${this.state.number}&size=10`);
        let getDealsHeaders = new Headers();
        getDealsHeaders.append('Content-Type', 'application/json');

        const options = {
            method: "GET",
            headers: getDealsHeaders
        };

        // this is the call to my API that returns an array of json objects
        return fetch(url, options)
            .then(function (response) {
                console.log('respon', response)
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                return response;
            })
            .then(results => {
                var records = results.json();
                return records;
            })
            .then(data => data);
    };




    onGridReady = params => {
        this.gridApi = params;
        this.gridColumnApi = params.columnApi;
        var data = this.dataSource();
        params.api.setDatasource(data);
    }

    componentWillMount() {
        let url = new URL(`https://api-dev.vuarausach.vn/products/customer/top/products`);
        let getDealsHeaders = new Headers();
        getDealsHeaders.append('Content-Type', 'application/json');

        const options = {
            method: "GET",
            headers: getDealsHeaders
        };

        return fetch(url, options)
            .then(function (response) {

                if (!response.ok) {
                    throw Error(response.statusText)
                }
                return response;
            })
            .then(results => {
                var records = results.json();
                return records;
            })
            .then(data => {
                console.log('444', data.data)
                this.setState({ products:data.data });
                return data
            });
    }

    render() {

        return (
            <Segment>
                {this.state.products && this.state.products.map((item, idx) => {
                    return (
                        <div key={idx}>
                            <div><span style={{marginRight: ' 10px'}}>{item.id}</span>{item.name}</div>
                        </div>
                    );
                })}
                <div>
                    <div style={{height: '800px', width: 'auto'}} className="ag-theme-balham">
                        {<AgGridReact onGridReady={function ($event) {
                            $event.api.sizeColumnsToFit();
                        }, this.onGridReady}
                                      columnDefs={this.state.columnDefs}
                                      gridOptions={this.props.gridOptions}
                                      modules={this.state.modules}
                                      components={this.state.components}
                                      debug={false}
                                      rowBuffer={this.state.rowBuffer}
                                      rowSelection={this.state.rowSelection}
                                      rowDeselection={true}
                                      rowModelType={this.state.rowModelType}
                                      cacheBlockSize={this.state.cacheBlockSize}
                                      cacheOverflowSize={this.state.cacheOverflowSize}
                                      maxConcurrentDatasourceRequests={this.state.maxConcurrentDatasourceRequests}
                                      infiniteInitialRowCount={this.state.infiniteInitialRowCount}
                                      maxBlocksInCache={this.state.maxBlocksInCache}
                        />}
                    </div>

                </div>
            </Segment>
        );
    }
}

export default App;
